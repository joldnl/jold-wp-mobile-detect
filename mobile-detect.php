<?php
/**
 * Plugin Name:     Jold: Mobile Detect
 * Plugin URI:      https://bitbucket.org/joldnl/jold-wp-mobile-detect/
 * Description:     Server side device detection plugin. Based on Mobile Detect.
 * Plugin Type:     mu-plugin
 *
 * Author:          Jurgen Oldenburg <info@jold.nl>
 * Author URI:      http://www.jold.nl
 *
 * Text Domain:     jold-wp-mobile-detect
 * Domain Path:     /languages
 * Version:         1.0.5
 *
 * @package         jold-mobile-detect
 *
 * Bitbucket Plugin URI: https://bitbucket.org/joldnl/jold-wp-mobile-detect/
 *
 */



/**
 * Only oad this class if WordPress is active
 */
if( !defined('WPINC') ) {
    exit( 'Do NOT access this file directly: ' . basename(__FILE__) );
}


/**
 * Load the Mobile Detect class
 */
if ( file_exists( plugin_dir_path( __FILE__ ) . 'detect-functions.php' ) ) {
    require_once( plugin_dir_path( __FILE__ ) . 'detect-functions.php' );
}




/**
 *
 * Define a session var to store device data
 *
 * @var Mobile_Detect
 *
 */
$detect = new Mobile_Detect();


/**
 * Set session variable for the device to false
 */
$_SESSION['isTablet']	= false;
$_SESSION['isMobile']	= false;
$_SESSION['isDesktop']	= false;


/**
 * Set session variable per device
 */
if( $detect->isTablet() ){
	$_SESSION['isTablet'] = true;
}
else if( $detect->isMobile() ){
	$_SESSION['isMobile'] = true;
}
else{
	$_SESSION['isDesktop'] = true; // if device can't be detected, assume it's desktop.
}



/**
 *
 * jold_snippet_detector
 *
 * Function to load (device specific) template snippets
 *
 * @type	function
 * @date	2017/03/09
 * @since	1.0.0
 * @author  Jurgen Oldenburg
 *
 * @param   string      $snippet        The name of the snippet
 * @param   array       $data           The data array
 * @param   string      $overflow       Overflow vlass tablet - desktop
 * @param   boolean     $return         Wehther to return the snippet
 *
 * @return  n/a
 *
 */
function jold_snippet_detector( $snippet, $data = array(), $overflow = false, $return = false ) {

	// Check if website is responsive else just load desktop files
	if( jold_is_responsive() ) {

		// Set variables used for snippet filename
		if( $_SESSION['isTablet'] ){

			$deviceClass = 'tablet';

		}
		if( $_SESSION['isMobile'] ){

			$deviceClass = 'mobile';

		}
		if( $_SESSION['isDesktop'] ){

			$deviceClass = 'desktop';

		}

		// Load snippet file
		if ( $deviceClass == 'tablet' && $overflow == 'tablet=>desktop' ) {

			return get_template_part( 'snippets/' . $snippet, 'desktop' );

		} elseif( $deviceClass == 'mobile') {

			return get_template_part( 'snippets/' . $snippet );

		} else {

			return get_template_part( 'snippets/' . $snippet, $deviceClass );
		}

	} else {

		return get_template_part('snippets/'. $snippet, 'desktop');

	}

}
