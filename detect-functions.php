<?php
/**
 *
 * Mobile detect functions.
 *
 * A serie of mobile detect functions. Based on the Mobile Detect plugin.
 *
 * @package      MobileDetect
 * @subpackage
 * @category     mobile
 * @author       Jurgen Oldenburg
 *
 */


new MobileDetect;


/**
 * Mobile Detect class
 */
class MobileDetect {


    /**
     *
     * __construct
     *
     * Main class costructor function
     *
     * @type	function
     * @date	2017/03/08
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    function __construct() {


        global $mobile_detect;


        // Load the Mobile_Detect.php class from vendor folder
        if( !class_exists('device_class') ) {
        	require_once( 'vendor/mobiledetect/mobiledetectlib/Mobile_Detect.php' );
        }

        // Get current user agent
        $useragent 		= isset( $_SERVER['HTTP_USER_AGENT'] ) ? $_SERVER['HTTP_USER_AGENT'] : "";

        // Invoke Mobile_Detect class as a variable
        $mobile_detect	= new Mobile_Detect();

        // Set Detection Type
        $mobile_detect->setDetectionType( 'extended' );

    }



    /**
     *
     * Detect iPhone device
     *
     * @since	1.0
     *
     * @param	n/a
     * @return	boolean		Returns true if device is iPhone
     *
     */
    public static function is_iphone() {

        global $mobile_detect;
        return $mobile_detect->isIphone();

    }



    /**
     *
     * Detect iPad device
     *
     * @since	1.0
     *
     * @param	n/a
     * @return	boolean		Returns true if device is iPad
     *
     */
    public static function is_ipad() {

        global $mobile_detect;
        return $mobile_detect->isIpad();

    }



    /**
     *
     * Detect iPod device
     *
     * @since	1.0
     *
     * @param	n/a
     * @return	boolean		Returns true if device is iPod
     *
     */
    public static function is_ipod() {

        global $mobile_detect;
        return $mobile_detect->is( 'iPod' );

    }



    /**
     *
     * Detect Android device
     *
     * @since	1.0
     *
     * @param	n/a
     * @return	boolean		Returns true if device is Android
     *
     */
    public static function is_android() {

        global $mobile_detect;
        return $mobile_detect->isAndroidOS();

    }



    /**
     *
     * Detect Blackberry device
     *
     * @since	1.0
     *
     * @param	n/a
     * @return	boolean		Returns true if device is Blackberry
     *
     */
    public static function is_blackberry() {
        global $mobile_detect;
        return $mobile_detect->isBlackBerry();
    }



    /**
     *
     * Detect Opera Mobile device
     *
     * @since	1.0
     *
     * @param	n/a
     * @return	boolean		Returns true if device is Opera Mobile
     *
     */
    public static function is_opera_mobile() {

        global $mobile_detect;
        return $mobile_detect->isOpera();

    }



    /**
     *
     * Detect webOS device such as Pre and Pixi
     *
     * @since	1.0
     *
     * @param	n/a
     * @return	boolean		Returns true if device is webOS
     *
     */
    public static function is_palm() {

        _deprecated_function( 'is_palm', '1.2', 'is_webos' );

        global $mobile_detect;
        return $mobile_detect->is( 'webOS' );

    }



    /**
     *
     * Detect webOS device such as Pre and Pixi
     *
     * @since	1.0
     *
     * @param	n/a
     * @return	boolean		Returns true if device is webOS
     *
     */
    public static function is_webos() {

        global $mobile_detect;
        return $mobile_detect->is( 'webOS' );

    }



    /**
     *
     * Detect Symbian device like Nokia smartphones
     *
     * @since	1.0
     *
     * @param	n/a
     * @return	boolean		Returns true if device is webOS
     *
     */
    public static function is_symbian() {

        global $mobile_detect;
        return $mobile_detect->is( 'Symbian' );

    }



    /**
     *
     * Detect Windows device
     *
     * @since	1.0
     *
     * @param	n/a
     * @return	boolean		Returns true if device is Windows
     *
     */
    public static function is_windows_mobile() {

        global $mobile_detect;
        return $mobile_detect->is( 'WindowsMobileOS' ) || $mobile_detect->is( 'WindowsPhoneOS' );

    }



    /**
     *
     * Detect LG Phone device
     *
     * @since	1.0
     *
     * @param	n/a
     * @return	boolean		Returns true if device is LG Phone
     *
     */
    public static function is_lg() {

        _deprecated_function( 'is_lg', '1.2' );

        global $useragent;
        return preg_match( '/LG/i', $useragent );

    }



    /**
     *
     * Detect Motorola smartphone device
     *
     * @since	1.0
     *
     * @param	n/a
     * @return	boolean		Returns true if device is Morotola
     *
     */
    public static function is_motorola() {

        global $mobile_detect;
        return $mobile_detect->is( 'Motorola' );

    }



    /**
     *
     * Detect Nokia device
     *
     * @since	1.0
     *
     * @param	n/a
     * @return	boolean		Returns true if device is Nokia
     *
     */
    public static function is_nokia() {

        _deprecated_function( 'is_nokia', '1.2' );

        global $useragent;
        return preg_match( '/Series60/i', $useragent ) || preg_match( '/Symbian/i', $useragent ) || preg_match( '/Nokia/i', $useragent );

    }



    /**
     *
     * Detect Samsung device
     *
     * @since	1.0
     *
     * @param	n/a
     * @return	boolean		Returns true if device is Samsung
     *
     */
    public static function is_samsung() {

        global $mobile_detect;
        return $mobile_detect->is( 'Samsung' );

    }



    /**
     *
     * Detect Galaxy Tab device
     *
     * @since	1.0
     *
     * @param	n/a
     * @return	boolean		Returns true if device is Galaxy Tab
     *
     */
    public static function is_samsung_galaxy_tab() {

        _deprecated_function( 'is_samsung_galaxy_tab', '1.2', 'is_samsung_tablet' );

        return is_samsung_tablet();

    }



    /**
     *
     * Detect Samsung Tab device
     *
     * @since	1.0
     *
     * @param	n/a
     * @return	boolean		Returns true if device is Samsung Tab
     *
     */
    public static function is_samsung_tablet() {

        global $mobile_detect;
        return $mobile_detect->is( 'SamsungTablet' );

    }



    /**
     *
     * Detect Amazon Kindle device
     *
     * @since	1.0
     *
     * @param	n/a
     * @return	boolean		Returns true if device is Amazon Kindle
     *
     */
    public static function is_kindle() {

        global $mobile_detect;
        return $mobile_detect->is( 'Kindle' );

    }



    /**
     *
     * Detect Sony Ericsson device
     *
     * @since	1.0
     *
     * @param	n/a
     * @return	boolean		Returns true if device is Sony Ericsson
     *
     */
    public static function is_sony_ericsson() {

        global $mobile_detect;
        return $mobile_detect->is( 'Sony' );

    }



    /**
     *
     * Detect Nintendo DS device
     *
     * @since	1.0
     *
     * @param	n/a
     * @return	boolean		Returns true if device is Nintendo DS
     *
     */
    public static function is_nintendo() {

        global $useragent;
        return preg_match( '/Nintendo DSi/i', $useragent ) || preg_match( '/Nintendo DS/i', $useragent );

    }



    /**
     *
     * Detect Smaertphone device
     *
     * @since	1.0
     *
     * @param	n/a
     * @return	boolean		Returns true if device is a Smartphone
     *
     */
    public static function is_smartphone() {

        global $mobile_detect;
        $grade = $mobile_detect->mobileGrade();

        if ( $grade == 'A' || $grade == 'B' ) {

            return true;

        } else {

            return false;

        }

    }



    /**
     *
     * Detect Handheld device
     *
     * @since	1.0
     *
     * @param	n/a
     * @return	boolean		Returns true if device is a Handheld device
     *
     */
    public static function is_handheld() {

        return $this->is_mobile() || $this->is_iphone() || $this->is_ipad() || $this->is_ipod() || $this->is_android() || $this->is_blackberry() || $this->is_opera_mobile() || $this->is_webos() || $this->is_symbian() || $this->is_windows_mobile() || $this->is_motorola() || $this->is_samsung() || $this->is_samsung_tablet() || $this->is_sony_ericsson() || $this->is_nintendo();

    }



    /**
     *
     * Detect a mobile device
     *
     * @since	1.0
     *
     * @param	n/a
     * @return	boolean		Returns true if device is a moble device
     *
     */
    public static function is_mobile() {

        global $mobile_detect;
        if ( self::is_tablet() ) return false;
        return $mobile_detect->isMobile();

    }



    /**
     *
     * Detect iOs/Apple device
     *
     * @since	1.0
     *
     * @param	n/a
     * @return	boolean		Returns true if device is a iOs/Apple
     *
     */
    public static function is_ios() {

        global $mobile_detect;
        return $mobile_detect->isiOS();

    }



    /**
     *
     * Detect Tablet device
     *
     * @since	1.0
     *
     * @param	n/a
     * @return	boolean		Returns true if device is a Tablet
     *
     */
    public static function is_tablet() {

        global $mobile_detect;
        return $mobile_detect->isTablet();

    }



    /**
     *
     * Detect Internet Explorer device
     *
     * @since	1.0
     *
     * @param	n/a
     * @return	boolean		Returns true if device is Internet Explorer
     *
     */
    public static function is_ie( $version_test = '' ) {

        // Check if the test version is set
        if( isset ( $version_test ) ) {

            preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);

            // Is this IE
            if (count($matches)>1){

                $version_actual = $matches[1]; // Set IE version

                // Check if actual version is equal to test version
                if( $version_actual == $version_test ) {

                    // echo "MSIE" . $version_actual;
                    return true;

                }

            }
        }
    }


}
